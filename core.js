const clima = require('./clima/clima');

const getInfo = async(direccion) =>{
  try
  {
    let climaMundo = await clima.getClima(direccion);

    return `El clima en ${climaMundo.ciudad} es: ${climaMundo.temp}°C`;
  }
  catch(e)
  {
    return e;
  }
}

module.exports = {
  getInfo
}