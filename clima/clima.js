const axios = require('axios');
const apis = require('../apiConfig');

const getClima = async(direccion) =>{
  let resp = await axios.get(`https://api.openweathermap.org/data/2.5/weather?q=${direccion}&units=metric&appid=${apis.weatherApi}`);

  if(resp.data.cod === 401)
  {
    throw new Error('Problema con la API, cuota Excedida')
  }

  if(resp.data.cod === 404)
  {
    throw new Error('Ciudad no localizada');
  }

  return {
    ciudad: resp.data.name,
    temp: resp.data.main.temp
  }
}

module.exports = {
  getClima
}